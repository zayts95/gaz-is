package com.gazis.test.zaytsevi.dao;

import com.gazis.test.zaytsevi.ConnectionPool;
import com.gazis.test.zaytsevi.model.Account;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class AccountDaoTest {
    public static final String INIT = "create table accounts (\n" +
            "id varchar(20) primary key, password varchar(20), name varchar(20), surname varchar(20), birth Date, phone varchar(15), email varchar(30), address varchar(100));\n" +
            "insert into accounts (id, password, name, surname, birth, phone, email, address) values('ivanov', 'password','Иван', 'Иванов','1995-11-26', '+79313586640','zayts95@gmail.com', 'Савушкина 122'),\n" +
            "('petrov', 'password','Петр', 'Петров','1995-11-26', '+79313586640','zayts95@gmail.com', 'Савушкина 122');";
    public static final String DESTROY = "drop table accounts";

    private final ConnectionPool connectionPool;
    private final AccountDao accountDao;
    private final Account ivanov;
    private final Account petrov;

    AccountDaoTest() throws IOException, SQLException, InterruptedException {
        connectionPool = ConnectionPool.getInstance();
        accountDao = new AccountDao(connectionPool);
        ivanov = new Account("ivanov", "password","Иван", "Иванов", new Date(817333200000L),
                "+79313586640","zayts95@gmail.com", "Савушкина 122");
        petrov = new Account("petrov", "password","Петр", "Петров", new Date(817333200000L),
                "+79313586640","zayts95@gmail.com", "Савушкина 122");
    }

    @BeforeEach
    void init() throws SQLException, InterruptedException {
        try (PreparedStatement preparedStatement = connectionPool.getConnection().prepareStatement(INIT)){
            preparedStatement.executeUpdate();
        }
        connectionPool.getConnection().commit();
    }

    @AfterEach
    void destroy() throws InterruptedException, SQLException {
        try (PreparedStatement preparedStatement = connectionPool.getConnection().prepareStatement(DESTROY)){
            preparedStatement.executeUpdate();
        }
        connectionPool.getConnection().commit();
        connectionPool.releaseConnection();
    }

    @Test
    void createNewAccount() throws InterruptedException, SQLException {
        Account account = new Account("test", "password","test", "test", new Date(817333200000L), "+79313586640","zayts95@gmail.com", "Савушкина 122");
        accountDao.createNewAccount(account);
        Optional<Account> optional = accountDao.getAccountById("test");
        optional.ifPresent(actual -> assertEquals(account, actual));
    }

    @Test
    void getAccountById() throws SQLException, InterruptedException {
        Optional<Account> notPresentOptional = accountDao.getAccountById("wrongtest");
        assertFalse(notPresentOptional.isPresent());
        Optional<Account> optional = accountDao.getAccountById("ivanov");
        optional.ifPresent(actual -> assertEquals(ivanov, actual));
    }

    @Test
    void getAllAccounts() throws SQLException, InterruptedException {
        List<Account> expected = new ArrayList<>();
        expected.add(ivanov);
        expected.add(petrov);
        List<Account> actual = accountDao.getAllAccounts();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    void updateAccount() throws SQLException, InterruptedException {
        ivanov.setName("newName");
        accountDao.updateAccount("ivanov", ivanov);
        Optional<Account> optional = accountDao.getAccountById("ivanov");
        optional.ifPresent(account -> assertEquals(ivanov, account));
    }
}