package com.gazis.test.zaytsevi;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class ConnectionPoolTest {

    static ConnectionPool connectionPool;

    static {
        try {
            connectionPool = ConnectionPool.getInstance();
        } catch (SQLException | IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    @Test
    void getConnectionWhenPoolIsEmptyWaitingForRelease() throws SQLException, InterruptedException {
        Runnable runnable1 = () -> {
            try {
                connectionPool.getConnection();
                Thread.sleep(10000);
                connectionPool.releaseConnection();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable1);
        Thread thread3 = new Thread(runnable1);
        Thread thread4 = new Thread(runnable1);
        Thread thread5 = new Thread(runnable1);
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        Thread.sleep(500);
        Connection connection = connectionPool.getConnection();
        assertTrue(connection.isValid(0));
        connectionPool.releaseConnection();
    }

    @Test
    void sameConnectionInOneThread() throws InterruptedException {
        Connection connection1 = connectionPool.getConnection();
        Connection connection2 = connectionPool.getConnection();
        assertSame(connection1, connection2);
        connectionPool.releaseConnection();
    }
}