package com.gazis.test.zaytsevi.dao;

import com.gazis.test.zaytsevi.ConnectionPool;
import com.gazis.test.zaytsevi.model.Account;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountDao {
    private final ConnectionPool connectionPool;

    public AccountDao(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    public void createNewAccount(Account account) throws InterruptedException, SQLException {
        String sql = "insert into accounts (id, password, name, surname, birth, phone, email, address) values(?, ?,?, ?,?, ?,?, ?)";
        try (PreparedStatement preparedStatement = connectionPool.getConnection().prepareStatement(sql)) {
            configurePreparedStatementForAccount(account, preparedStatement);
            preparedStatement.executeUpdate();
        }
    }

    public Optional<Account> getAccountById(String id) throws SQLException, InterruptedException {
        String sql = "Select * from accounts where id = ?";
        Account account = null;
        try (PreparedStatement preparedStatement = connectionPool.getConnection().prepareStatement(sql)) {
            preparedStatement.setString(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    account = createAccountFromResultSet(resultSet);
                }
            }
        }
        return Optional.ofNullable(account);
    }

    public List<Account> getAllAccounts() throws InterruptedException, SQLException {
        List<Account> accounts = new ArrayList<>();
        String sql = "select * from accounts";
        try (ResultSet resultSet = connectionPool.getConnection().prepareStatement(sql).executeQuery()) {
            while (resultSet.next()) {
                accounts.add(createAccountFromResultSet(resultSet));
            }
        }
        return accounts;
    }

    public void updateAccount(String oldId, Account account) throws InterruptedException, SQLException {
        String sql = "update accounts set id=?, password = ?, name = ?, surname = ?, birth = ?, phone = ?, email=?, address = ? where id=?";
        try (PreparedStatement preparedStatement = connectionPool.getConnection().prepareStatement(sql)) {
            configurePreparedStatementForAccount(account, preparedStatement);
            preparedStatement.setString(9, oldId);
            preparedStatement.executeUpdate();
        }
    }

    private void configurePreparedStatementForAccount(Account account, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, account.getId());
        preparedStatement.setString(2, account.getPassword());
        preparedStatement.setString(3, account.getName());
        preparedStatement.setString(4, account.getSurname());
        preparedStatement.setDate(5, account.getBirthDate());
        preparedStatement.setString(6, account.getPhone());
        preparedStatement.setString(7, account.getEmail());
        preparedStatement.setString(8, account.getAddress());
    }

    private Account createAccountFromResultSet(ResultSet resultSet) throws SQLException {
        return new Account(resultSet.getString("id"), resultSet.getString("password"),
                resultSet.getString("name"), resultSet.getString("surname"),
                resultSet.getDate("birth"), resultSet.getString("phone"),
                resultSet.getString("email"), resultSet.getString("address"));
    }
}
