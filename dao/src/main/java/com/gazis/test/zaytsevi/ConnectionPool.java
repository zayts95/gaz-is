package com.gazis.test.zaytsevi;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * Thread-safe connection pool. It's used to be sure that dao layer and service layer will use same connection in one
 * thread, to make service layer transactions possible.
 */
public class ConnectionPool {
    public static final String PROPERTIES_NAME = "database.properties";
    private final BlockingQueue<Connection> availableConnections;

    private final ThreadLocal<Connection> localConnection;
    private static ConnectionPool instance;

    private ConnectionPool(String propertiesName) throws IOException, SQLException, InterruptedException {
        Properties properties = new Properties();
        properties.load(ConnectionPool.class.getClassLoader().getResourceAsStream(propertiesName));
        String url = properties.getProperty("database.url");
        String user = properties.getProperty("database.user");
        String password = properties.getProperty("database.password");
        int maxConnections = Integer.parseInt(properties.getProperty("database.max_connections"));
        availableConnections = new LinkedBlockingQueue<>(maxConnections);
        for (int i = 0; i < maxConnections; i++) {
            Connection connection = DriverManager.getConnection(url, user, password);
            connection.setAutoCommit(false);
            availableConnections.put(connection);
        }
        localConnection = new ThreadLocal<>();
    }

    /**
     * Singleton pattern realization for ConnectionPool
     * @return Singleton instance of connection pool
     */
    public static ConnectionPool getInstance() throws IOException, SQLException, InterruptedException {
        if (instance == null) {
            instance = new ConnectionPool(PROPERTIES_NAME);
        }
        return instance;
    }

    /**
     * Retrieves connection from ThreadLocal if there is one, otherwise retrieves it from BlockingQueue (connection pool)
     * @return a Connection object that is a physical connection to the database
     */
    public Connection getConnection() throws InterruptedException {
        if (localConnection.get() == null) {
            Connection connection = availableConnections.take();
            localConnection.set(connection);
            return connection;
        } else {
            return localConnection.get();
        }
    }

    /**
     * puts connection from ThreadLocal to connection pool if there is one
     */
    public void releaseConnection() throws InterruptedException {
        Connection connection = localConnection.get();
        if (connection!=null){
            availableConnections.put(connection);
            localConnection.remove();
        }
    }
}
