package com.gazis.test.zaytsevi.service;

import com.gazis.test.zaytsevi.ConnectionPool;
import com.gazis.test.zaytsevi.dao.AccountDao;
import com.gazis.test.zaytsevi.model.Account;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class AccountServiceTest {

    private final Account ivanov = new Account("ivanov", "password","Иван", "Иванов", new Date(817333200000L),
                "+79313586640","zayts95@gmail.com", "Савушкина 122");
    private final Account petrov = new Account("petrov", "password","Петр", "Петров", new Date(1132952400000L),
            "+79313586640","zayts95@gmail.com", "Савушкина 122");
    private final Account zaytsev = new Account("zaytsev", "password","Иван", "Зайцев", new Date(817333200000L),
            "+79313586640","zayts95@gmail.com", "Савушкина 122");

    private final AccountDao accountDao;
    private final AccountService accountService;

    public AccountServiceTest() throws IOException, SQLException, InterruptedException {
        this.accountDao = Mockito.mock(AccountDao.class);
        this.accountService = new AccountService(ConnectionPool.getInstance(), accountDao);
    }

    @Test
    void createAccount() throws SQLException, InterruptedException {
        accountService.createAccount(zaytsev);
        verify(accountDao).createNewAccount(zaytsev);
    }

    @Test
    void getAccountById() throws SQLException, InterruptedException {
        accountService.getAccountById("zaytsev");
        verify(accountDao).getAccountById("zaytsev");
    }

    @Test
    void getAllAccounts() throws SQLException, InterruptedException {
        accountService.getAllAccounts();
        verify(accountDao).getAllAccounts();
    }

    @Test
    void updateAccount() throws SQLException, InterruptedException {
        accountService.updateAccount("zaytsev", zaytsev);
        verify(accountDao).updateAccount("zaytsev", zaytsev);
    }

    @Test
    void getYoungAccounts() throws SQLException, InterruptedException {
        List<String> expected = new ArrayList<>();
        expected.add("petrov");
        when(accountDao.getAllAccounts()).thenReturn(Arrays.asList(zaytsev, petrov, ivanov));
        List<String> actual = accountService.getYoungAccounts(20);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    void accountsSurnameEndsWith() throws SQLException, InterruptedException {
        when(accountDao.getAllAccounts()).thenReturn(Arrays.asList(zaytsev, petrov, ivanov));
        long actual = accountService.accountsSurnameEndsWith("ов");
        assertEquals(2, actual);
    }
}