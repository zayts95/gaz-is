package com.gazis.test.zaytsevi.service;

import com.gazis.test.zaytsevi.ConnectionPool;
import com.gazis.test.zaytsevi.dao.AccountDao;
import com.gazis.test.zaytsevi.model.Account;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AccountService {

    private final ConnectionPool connectionPool;
    private final AccountDao accountDao;

    public AccountService() throws IOException, SQLException, InterruptedException {
        this(ConnectionPool.getInstance(), new AccountDao(ConnectionPool.getInstance()));
    }

    public AccountService(ConnectionPool connectionPool) {
        this(connectionPool, new AccountDao(connectionPool));
    }

    public AccountService(ConnectionPool connectionPool, AccountDao accountDao) {
        this.connectionPool = connectionPool;
        this.accountDao = accountDao;
    }

    /**
     * creates a new row in table 'accounts' which will be a representation of an account param
     * @param account
     * @throws SQLException – if a database error occurs
     * @throws InterruptedException – if interrupted while waiting
     */
    public void createAccount(Account account) throws SQLException, InterruptedException {
        transaction(() -> accountDao.createNewAccount(account));
    }

    /**
     * reads a row from table 'accounts' which will be found by an id param
     * @param id - primary key in table 'accounts'
     * @return Optional<Account> which will contain Account instance if one will be found
     * @throws InterruptedException – if interrupted while waiting
     * @throws SQLException – if a database error occurs
     */
    public Optional<Account> getAccountById(String id) throws InterruptedException, SQLException {
        try {
            return accountDao.getAccountById(id);
        } finally {
            connectionPool.releaseConnection();
        }
    }

    /**
     * Retrieves list of all accounts represented in a table 'accounts'
     * @return list of all accounts represented in a table 'accounts'
     * @throws SQLException – if a database error occurs
     * @throws InterruptedException – if interrupted while waiting
     */
    public List<Account> getAllAccounts() throws SQLException, InterruptedException {
        try {
            return accountDao.getAllAccounts();
        } finally {
            connectionPool.releaseConnection();
        }
    }

    /**
     * Updates row from table 'accounts' which will be found by primary key id = oldId
     * @param oldId - primary key in table 'accounts' for row to update
     * @param account - contains new values for the row
     * @throws SQLException – if a database error occurs
     * @throws InterruptedException – if interrupted while waiting
     */
    public void updateAccount(String oldId, Account account) throws SQLException, InterruptedException {
        transaction(() -> accountDao.updateAccount(oldId, account));
    }

    /**
     * Retrieves list of IDs from table 'accounts' with age under neededAge
     * @param neededAge upper border for accounts age
     * @return list of IDs from table 'accounts' with age under neededAge
     * @throws SQLException – if a database error occurs
     * @throws InterruptedException – if interrupted while waiting
     */
    public List<String> getYoungAccounts(int neededAge) throws SQLException, InterruptedException {
        try {
            List<Account> accounts = accountDao.getAllAccounts();
            return accounts.stream().filter((account -> getAge(account) < neededAge)).map(Account::getId).collect(Collectors.toList());
        } finally {
            connectionPool.releaseConnection();
        }
    }

    /**
     * Counts amount of accounts having surnames ends with suffix
     * @param suffix - suffix that accounts surname have to end with to be counted
     * @return amount of accounts having surnames ends with suffix
     * @throws SQLException – if a database access error occurs
     * @throws InterruptedException – if interrupted while waiting
     */
    public long accountsSurnameEndsWith(String suffix) throws SQLException, InterruptedException {
        try {
            List<Account> accounts = accountDao.getAllAccounts();
            return accounts.stream().filter(account -> account.getSurname().endsWith(suffix)).count();
        } finally {
            connectionPool.releaseConnection();
        }
    }

    private int getAge(Account account) {
        return Period.between(account.getBirthDate().toLocalDate(), LocalDate.now()).getYears();
    }

    private void transaction(Transaction transaction) throws InterruptedException, SQLException {
        Connection connection = connectionPool.getConnection();
        try {
            transaction.execute();
            connection.commit();
        } catch (SQLException throwable) {
            connection.rollback();
        } finally {
            connectionPool.releaseConnection();
        }
    }

    @FunctionalInterface
    private interface Transaction {
        void execute() throws InterruptedException, SQLException;
    }
}
